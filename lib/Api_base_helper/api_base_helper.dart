//@dart=2.1

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restorma/helper/helper.dart';
import 'package:restorma/model/auhtaenicate.dart';
import 'package:http/http.dart' as http;
import 'package:restorma/model/change_password.dart';
import 'package:restorma/model/login_response.dart';
import 'package:restorma/model/products.dart';
import 'package:restorma/model/table.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiBaseHelper {
  Helper _helper = Helper();
  Future<dynamic> authenticateUser(BuildContext context, String email,
      String password, String usercode) async {
    int statusCode;
    try {
      final response = await http.post(
          Uri.parse("https://restorma.com/api/login"),
          headers: {"Content-Type": "application/json"},
          body: jsonEncode(new UserLogin(
              email: email, password: password, userCode: usercode)));

      final responseData = json.decode(response.body);
      if (responseData['status'] == 'Success') {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Success fully signed in'),
          duration: Duration(seconds: 1),
        ));
      } else {
        showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text('An Error Occurred!'),
                  content: Text('Something Went Wrong!!'),
                  actions: [
                    FlatButton(
                      child: Text('okay'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ],
                ));
      }
      statusCode = response.statusCode;

      if (response.statusCode == 200) {
        var resp = json.decode(response.body.toString());

        LoginUserResponse loginUserResponse = LoginUserResponse.fromJson(resp);

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('token', loginUserResponse.data.token ?? '');
      }
    } on SocketException {
      // Fluttertoast.showToast(msg: 'No Interet connetion');
    }
    print(statusCode);
    return statusCode;
  }

  Future<dynamic> changePassword(BuildContext context, String oldPasswor,
      String newPassword, String token) async {
    Map<String, String> headers = {
      'Content-Type': 'application/json;charset=UTF-8',
      'Charset': 'utf-8'
    };
    int statusCode;
    try {
      final response = await http.post(Uri.parse(token),
          headers: headers,
          body: jsonEncode(new ChangePassword(
              oldPassword: oldPasswor, newPassword: newPassword)));
      var pdfText = await json.decode(json.encode(response.body));
      print(pdfText);

      final responseData = json.decode(response.body);
      if (responseData['status'] == 'Success') {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Success fully signed in'),
          duration: Duration(seconds: 1),
        ));
      } else {
        showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text('An Error Occurred!'),
                  content: Text('Something Went Wrong!!'),
                  actions: [
                    FlatButton(
                      child: Text('okay'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ],
                ));
      }
      statusCode = response.statusCode;

      if (response.statusCode == 200) {
        var resp = json.decode(response.body.toString());

        LoginUserResponse loginUserResponse = LoginUserResponse.fromJson(resp);

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('token', loginUserResponse.data.token ?? '');
      }
    } on SocketException {
      // Fluttertoast.showToast(msg: 'No Interet connetion');
    }
    print(statusCode);
    return statusCode;
  }
  //
  // Future<dynamic> changePassword(BuildContext context, String oldPasswor,
  //     String newPassword, String token) async {
  //   int? statusCode;
  //   // print(token);
  //
  //   try {
  //     final response = await http.post(
  //         Uri.parse(
  //             "https://restorma.com/api/load-order?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MzY0MTYwMDAsImV4cGlyZSI6MTYzNjQxNjAwMCwiZGJ5ZWFyIjoiMjAyMSIsImNvZGUiOiJkZW1vIiwiaWQiOjEwfQ._S_U2_LJGXpHy0uN9kMclXoLXEHPQNv56Jj26vzRYV8"),
  //         headers: {
  //           "Content-Type": "application/json",
  //         },
  //         body: jsonEncode(ChangePassword(
  //             oldPassword: oldPasswor, newPassword: newPassword)));
  //     print('a');
  //     print(response.statusCode);
  //     print(response.body);
  //     final responseData = json.decode(response.body);
  //
  //     if (responseData == 'success') {
  //       Scaffold.of(context).showSnackBar(SnackBar(
  //         content: Text('Success fully signed in'),
  //         duration: Duration(seconds: 1),
  //       ));
  //     } else {
  //       showDialog(
  //           context: context,
  //           builder: (ctx) => AlertDialog(
  //                 title: Text('An Error Occurred!'),
  //                 content: Text("responseData['data']['message']"),
  //                 actions: [
  //                   FlatButton(
  //                     child: Text('okay'),
  //                     onPressed: () {
  //                       Navigator.pop(context);
  //                     },
  //                   )
  //                 ],
  //               ));
  //     }
  //     statusCode = response.statusCode;
  //
  //     if (response.statusCode == 200) {}
  //   } on SocketException {
  //     // Fluttertoast.showToast(msg: 'No Interet connetion');
  //   }
  //   print(statusCode);
  //   return statusCode;
  // }
  //

  Future<List<Products>> getProducts(String url) async {
    final response = await _helper.getData(url);
    var list = List<Products>.from(response.map((c) => Products.fromJson(c)));
    return list;
  }

  Future<List<Tables>> getTables(String url) async {
    final response = await _helper.getData(url);
    var list = List<Tables>.from(response.map((c) => Tables.fromJson(c)));
    return list;
  }
}
