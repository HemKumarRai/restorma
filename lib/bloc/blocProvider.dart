//@dart=2.1

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restorma/Api_base_helper/api_base_helper.dart';
import 'package:restorma/Api_base_helper/api_response.dart';
import 'package:restorma/model/products.dart';
import 'package:restorma/screen/signin_screen.dart';

class Bloc {
  StreamController _productController;
  StreamController _tableCOntroller;
  StreamSink<ApiResponse<List<Products>>> get productSink =>
      _productController.sink;

  Stream<ApiResponse<List<Products>>> get productStream =>
      _productController.stream;

  fetchProducts(BuildContext context, String url) async {
    _productController = StreamController<ApiResponse<List<Products>>>();
    ApiBaseHelper _repository = ApiBaseHelper();

    productSink.add(ApiResponse.loading('Fetching Attendance'));

    try {
      List<Products> list = (await _repository.getProducts(url));
      productSink.add(ApiResponse.completed(list));
    } catch (e) {
      productSink.add(ApiResponse.error(e.toString()));
      print(e.toString());
      if (e.toString() == 'Constants.UNAUTHORIZED_401') {
        productSink.add(ApiResponse.error('Session Expired'));
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SignInScreen()));
      }
    }
    _productController.close();
  }
}
