
//@dart=2.1

class AppException implements Exception {
  final _message;
  final _prefix;

  AppException([this._message, this._prefix]);

  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends AppException {
  FetchDataException([String message])
      : super(message, 'ERROR_DURING_COMMUNICATION');
}

class BadRequestException extends AppException {
  BadRequestException([message]) : super(message, "Invalid Request");
}

class UnauthorisedException extends AppException {
  UnauthorisedException([message]) : super(message, "UnAuthorized");
}

class InvalidInputException extends AppException {
  InvalidInputException([String message]) : super(message, 'nvalid Input');
}
