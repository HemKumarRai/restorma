//@dart=2.1

import 'dart:convert';

import 'package:flutter/cupertino.dart';

import 'package:http/http.dart' as http;

import 'app_exception.dart';
import 'message_dialog.dart';

class ErrorHandle {
  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body);
        return responseJson;
      case 201:
        var responseJson = json.decode(response.body);
        return responseJson;
        break;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        print('Token Expired : ${response.statusCode}');

        throw UnauthorisedException('${response.statusCode}');
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 404:
        throw InvalidInputException(response.body.toString());
      case 500:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
      case 501:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
      case 502:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  errorResponse(BuildContext context, http.Response response) {
    if (response.statusCode >= 400 && response.statusCode < 500) {
      try {
        var errorJson = jsonDecode(response.body); // import 'dart:convert';

        String message = errorJson["error"].values.toList()[0];
        MessageDialog().showErrorMessageDialog(context, message, 400);

        // print(error);
      } catch (e) {
        print(e);
      }
    } else if (response.statusCode >= 500) {
      try {
        var errorJson = jsonDecode(response.body); // import 'dart:convert';

        String message = errorJson["error"].values.toList()[0];
        MessageDialog().showErrorMessageDialog(context, message, 500);

        // print(error);
      } catch (e) {
        print(e);
      }
    }
  }
}
