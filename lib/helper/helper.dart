//@dart=2.1

import 'dart:io';

import 'package:http/http.dart' as http;

import 'app_exception.dart';
import 'error_handle.dart';

class Helper {
  Future<dynamic> getData(String url) async {
    var responseJson;
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "meta-name": "robots",
        "content": "noindex,nofollow"
      });
      print(response.body);

      responseJson = ErrorHandle().returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }
    print('api get recieved!');
    return responseJson;
  }
}
