import 'package:flutter/material.dart';

class ShowSnackBar {
  void showSnackBar(BuildContext context, message) {
    final snackbar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'Ok',
        textColor: Colors.white,
        onPressed: () {
          // Some code to undo the change.
        },
      ),
      duration: const Duration(milliseconds: 2000),
    );

    Scaffold.of(context).showSnackBar(snackbar);
  }
}
