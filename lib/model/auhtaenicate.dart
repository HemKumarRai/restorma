class UserLogin {
  String? email;
  String? password;
  String? userCode;

  UserLogin({this.email, this.password, this.userCode});

  UserLogin.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    userCode = json['user_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    data['user_code'] = this.userCode;
    return data;
  }
}
