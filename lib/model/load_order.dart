//@dart=2.1

class LoadOrder {
  Data data;
  String status;

  LoadOrder({this.data, this.status});

  LoadOrder.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['status'] = this.status;
    return data;
  }
}

class Data {
  OrderObj orderObj;

  Data({this.orderObj});

  Data.fromJson(Map<String, dynamic> json) {
    orderObj = json['orderObj'] != null
        ? new OrderObj.fromJson(json['orderObj'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderObj != null) {
      data['orderObj'] = this.orderObj.toJson();
    }
    return data;
  }
}

class OrderObj {
  int orderId;
  String guestId;
  String orderNo;
  String orderDate;
  String orderPartyType;
  String tableId;
  String roomId;
  String orderPartyAddress;
  String phoneNumber;
  String orderPartyName;
  String status;
  String paymentStatus;
  String notes;
  String createdAt;
  String updatedAt;
  String createdBy;
  String updatedBy;
  List<OrderItems> orderItems;

  OrderObj(
      {this.orderId,
      this.guestId,
      this.orderNo,
      this.orderDate,
      this.orderPartyType,
      this.tableId,
      this.roomId,
      this.orderPartyAddress,
      this.phoneNumber,
      this.orderPartyName,
      this.status,
      this.paymentStatus,
      this.notes,
      this.createdAt,
      this.updatedAt,
      this.createdBy,
      this.updatedBy,
      this.orderItems});

  OrderObj.fromJson(Map<String, dynamic> json) {
    orderId = json['order_id'];
    guestId = json['guest_id'];
    orderNo = json['order_no'];
    orderDate = json['order_date'];
    orderPartyType = json['order_party_type'];
    tableId = json['table_id'];
    roomId = json['room_id'];
    orderPartyAddress = json['order_party_address'];
    phoneNumber = json['phone_number'];
    orderPartyName = json['order_party_name'];
    status = json['status'];
    paymentStatus = json['payment_status'];
    notes = json['notes'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    if (json['order_items'] != null) {
      orderItems = new List<OrderItems>();
      json['order_items'].forEach((v) {
        orderItems.add(new OrderItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_id'] = this.orderId;
    data['guest_id'] = this.guestId;
    data['order_no'] = this.orderNo;
    data['order_date'] = this.orderDate;
    data['order_party_type'] = this.orderPartyType;
    data['table_id'] = this.tableId;
    data['room_id'] = this.roomId;
    data['order_party_address'] = this.orderPartyAddress;
    data['phone_number'] = this.phoneNumber;
    data['order_party_name'] = this.orderPartyName;
    data['status'] = this.status;
    data['payment_status'] = this.paymentStatus;
    data['notes'] = this.notes;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    if (this.orderItems != null) {
      data['order_items'] = this.orderItems.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderItems {
  int orderItemId;
  String orderId;
  String particular;
  String dishItemId;
  String quantity;
  String rate;
  String amount;
  String narration;
  Null status;
  String createdAt;
  String updatedAt;
  String createdBy;
  String updatedBy;

  OrderItems(
      {this.orderItemId,
      this.orderId,
      this.particular,
      this.dishItemId,
      this.quantity,
      this.rate,
      this.amount,
      this.narration,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.createdBy,
      this.updatedBy});

  OrderItems.fromJson(Map<String, dynamic> json) {
    orderItemId = json['order_item_id'];
    orderId = json['order_id'];
    particular = json['particular'];
    dishItemId = json['dish_item_id'];
    quantity = json['quantity'];
    rate = json['rate'];
    amount = json['amount'];
    narration = json['narration'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_item_id'] = this.orderItemId;
    data['order_id'] = this.orderId;
    data['particular'] = this.particular;
    data['dish_item_id'] = this.dishItemId;
    data['quantity'] = this.quantity;
    data['rate'] = this.rate;
    data['amount'] = this.amount;
    data['narration'] = this.narration;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}
