class LoginUserResponse {
  Data? data;
  String? status;

  LoginUserResponse({this.data, this.status});

  LoginUserResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['status'] = this.status;
    return data;
  }
}

class Data {
  String? token;
  int? id;
  String? name;
  String? phoneNumber;
  String? image;
  String? email;
  String? role;
  String? age;

  String? gender;
  String? maritalStatus;

  Data(
      {this.token,
      this.id,
      this.name,
      this.phoneNumber,
      this.image,
      this.email,
      this.role,
      this.age,
      this.gender,
      this.maritalStatus});

  Data.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    id = json['id'];
    name = json['name'];
    phoneNumber = json['phone_number'];
    image = json['image'];
    email = json['email'];
    role = json['role'];
    age = json['age'];

    gender = json['gender'];
    maritalStatus = json['marital_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone_number'] = this.phoneNumber;
    data['image'] = this.image;
    data['email'] = this.email;
    data['role'] = this.role;
    data['age'] = this.age;

    data['gender'] = this.gender;
    data['marital_status'] = this.maritalStatus;
    return data;
  }
}
