//@dart=2.1
class Products {
  List<Data> data;
  String status;

  Products({this.data, this.status});

  Products.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      if (data != null) {
        data = new List<Data>();
      }
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class Data {
  int id;
  String name;
  String slug;
  String createdAt;
  String updatedAt;
  String createdBy;
  String updatedBy;
  String productUrl;

  Data(
      {this.id,
      this.name,
      this.slug,
      this.createdAt,
      this.updatedAt,
      this.createdBy,
      this.updatedBy,
      this.productUrl});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    productUrl = json['product_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['product_url'] = this.productUrl;
    return data;
  }
}
