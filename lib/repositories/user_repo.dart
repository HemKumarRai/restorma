import 'package:flutter/cupertino.dart';
import 'package:restorma/Api_base_helper/api_base_helper.dart';

class Repository {
  final ApiBaseHelper _helper = ApiBaseHelper();

  Future<int> login(BuildContext context, String email, String password,
      String usercode) async {
    final int statusCode =
        await _helper.authenticateUser(context, email, password, usercode);
    return statusCode;
  }

  Future<int> changePassword(
    BuildContext context,
    String oldPw,
    String newPw,
    String token,
  ) async {
    final int statusCode =
        await _helper.changePassword(context, oldPw, newPw, token);
    return statusCode;
  }
}
