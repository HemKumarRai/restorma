import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:restorma/Api_base_helper/api_response.dart';
import 'package:restorma/bloc/blocProvider.dart';
import 'package:restorma/model/products.dart';

import 'orders_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  TabController? tabController;
  String? a = 'a';
  bool starter = true;
  bool mains = false;
  Bloc _bloc = new Bloc();
  @override
  void initState() {
    _bloc.fetchProducts(context,
        "https://restorma.com/api/categories?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MzY0MTYwMDAsImV4cGlyZSI6MTYzNjQxNjAwMCwiZGJ5ZWFyIjoiMjAyMSIsImNvZGUiOiJkZW1vIiwiaWQiOjEwfQ._S_U2_LJGXpHy0uN9kMclXoLXEHPQNv56Jj26vzRYV8");
    tabController = new TabController(length: 7, vsync: this)
      ..addListener(() {
        setState(() {
          switch (tabController!.index) {
            case 0:
              a = 'a';
              break;
            case 1:
              a = 'b';
              break;
            case 2:
              a = 'c';
              break;
            case 3:
              a = 'd';
              break;
            case 4:
              a = 'e';
              break;
            case 5:
              a = 'f';
              break;
            case 6:
              a = 'g';
              break;
          }
        });
      });
    super.initState();

    // print(userProfile);
  }

  List<String> _locations = ['A', 'B', 'C', 'D']; // Option 2
  String? _selectedLocation;
  int quantity = 6;
  int index = 0;

  Widget _side() {
    if (a == 'a') {
      return _starters();
    } else if (a == 'b') {
      return Container();
    } else if (a == 'c') {
      return mainss();
    } else if (a == 'd') {
      return Container();
    } else if (a == 'e') {
      return Container();
    } else if (a == 'f') {
      return Container();
    }

    return Container();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        body: StreamBuilder<ApiResponse<List<Products>>>(
          stream: _bloc.productStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data!.status) {
                case Status.COMPLETED:
                  return Container(
                    height: height,
                    child: ListView(
                      children: [
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Column(
                            children: [
                              Container(
                                height: 80,
                                width: width,
                                alignment: Alignment.centerRight,
                                color: Colors.red,
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        OrderScreen(
                                                          key: widget.key,
                                                        )));
                                          },
                                          child: _container('Orders')),
                                      _container('Take Orders'),
                                      _container('Welcome Mr. Bhatta')
                                    ],
                                  ),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 70,
                                        margin: EdgeInsets.only(
                                            top: 14, left: 8, right: 8),
                                        width: width * 60 / 100,
                                        color: Colors.white,
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(
                                            children: [
                                              Container(
                                                height: 40,
                                                margin: EdgeInsets.only(
                                                    left: 8, right: 12),
                                                width: width * 60 / 100 - 154,
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        width: 2)),
                                                child: TextField(
                                                  decoration: InputDecoration(
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Colors.green,
                                                          width: 0),
                                                    ),
                                                    enabledBorder:
                                                        OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Color(0xffB8C1EC),
                                                          width: 0),
                                                    ),
                                                    border:
                                                        OutlineInputBorder(),
                                                  ),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: 'Narrow',
                                                      fontSize: 16),
                                                ),
                                              ),
                                              Container(
                                                height: 40,
                                                color: Colors.red,
                                                margin:
                                                    EdgeInsets.only(right: 12),
                                                alignment: Alignment.center,
                                                width: 120,
                                                child: Text(
                                                  'Search',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 17),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Container(
                                        width: width * 60 / 100,
                                        margin: EdgeInsets.symmetric(
                                            vertical: 4, horizontal: 8),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 8, horizontal: 8),
                                        color: Colors.white,
                                        child: Column(
                                          children: [
                                            Container(
                                              height: 40,
                                              child: TabBar(
                                                controller: tabController,
                                                onTap: (index) {
                                                  setState(() {
                                                    index =
                                                        tabController!.index;
                                                  });
                                                },
                                                labelColor: Colors.white,
                                                labelStyle: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16),
                                                unselectedLabelColor:
                                                    Colors.black,
                                                indicator: BoxDecoration(
                                                    color: Colors.blue),
                                                isScrollable: true,
                                                tabs: <Widget>[
                                                  Tab(
                                                    text: 'Starters',
                                                  ),
                                                  Tab(
                                                    text: 'Snacks',
                                                  ),
                                                  Tab(
                                                    text: 'Mains',
                                                  ),
                                                  Tab(
                                                    text: 'Drinks',
                                                  ),
                                                  Tab(
                                                    text: 'Liquors',
                                                  ),
                                                  Tab(
                                                    text: 'Soups',
                                                  ),
                                                  Tab(
                                                    text: 'Coffee and Tea',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                              height: 12,
                                            ),
                                            Container(
                                              height: width,
                                              child: TabBarView(
                                                controller: tabController,
                                                children: [
                                                  _starter(),
                                                  Text(a ?? ''),
                                                  _mains(),
                                                  Text(a ?? ''),
                                                  Text(a ?? ''),
                                                  Text(a ?? ''),
                                                  Text(
                                                    a ?? '',
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  _side()
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                  break;
                case Status.LOADING:
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                  break;
                case Status.ERROR:
                  return Center(
                    child: Text(snapshot.data!.message),
                  );
                  break;
                default:
                  return Text('Products');
                  break;
              }
            }
            return Text('no Data');
          },
        ));
  }

  Widget _starter() {
    final width = MediaQuery.of(context).size.width;
    final ht = MediaQuery.of(context).size.height;
    return Container(
      width: width * 60 / 100,
      height: width,
      child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 3,
              childAspectRatio: 8 / 9),
          // physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: 10,
          itemBuilder: (context, index) => Container(
                child: GridTile(
                  child: Image.asset(
                    'assets/images/si_sticker.JPG',
                    fit: BoxFit.cover,
                  ),
                  footer: GridTileBar(
                    backgroundColor: Colors.black38,
                    title: Column(
                      children: [
                        Text(
                          'French Fries',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          '140.00',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
              )),
    );
  }

  Widget _starters() {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 14),
      child: Card(
        elevation: 5,
        borderOnForeground: true,
        shadowColor: Colors.red,
        child: Container(
          width: width * 40 / 100 - 92,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          margin: EdgeInsets.symmetric(horizontal: 22),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                _dropDown('Bill To', "--Walk In Customer--", true),
                _dropDown('Select Order Type', "New Order", false),
                _dropDown('Table Name/NUmber', '--Select Table--', false),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      _a('S.N'),
                      _a('Particular'),
                      _a("Quantity"),
                      _a('Price'),
                      _a('Total'),
                      _a('  '),
                    ],
                  ),
                ),
                Container(
                  height: 46,
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  color: Colors.grey,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        _b('1'),
                        SizedBox(
                          width: 30,
                        ),
                        _b('     Zhalfrezi'),
                        SizedBox(
                          width: 32,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              quantity++;
                            });
                          },
                          child: Container(
                            color: Colors.blue,
                            padding: EdgeInsets.symmetric(horizontal: 6),
                            child: Text(
                              '+',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        _b('  $quantity  '),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              quantity--;
                            });
                          },
                          child: Container(
                            color: Colors.red,
                            padding: EdgeInsets.symmetric(horizontal: 6),
                            child: Text(
                              '-',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 19,
                        ),
                        _b('477'),
                        SizedBox(
                          width: 19,
                        ),
                        _b('1042'),
                        SizedBox(
                          width: 19,
                        ),
                        Icon(
                          Icons.delete_forever,
                          color: Colors.red,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 46,
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  color: Colors.white,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        Container(
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            decoration: BoxDecoration(
                              border: Border(
                                right: BorderSide(color: Colors.grey, width: 2),
                              ),
                            ),
                            child: _b('  1  ')),
                        _b('  Zhalfrezi'),
                        SizedBox(
                          width: 32,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              quantity++;
                            });
                          },
                          child: Container(
                            color: Colors.blue,
                            padding: EdgeInsets.symmetric(horizontal: 6),
                            child: Text(
                              '+',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        _b('  $quantity  '),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              quantity--;
                            });
                          },
                          child: Container(
                            color: Colors.red,
                            padding: EdgeInsets.symmetric(horizontal: 6),
                            child: Text(
                              '-',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 19,
                        ),
                        _b('987'),
                        SizedBox(
                          width: 19,
                        ),
                        _b('1942'),
                        SizedBox(
                          width: 19,
                        ),
                        Icon(
                          Icons.delete_forever,
                          color: Colors.red,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 46,
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  color: Colors.grey,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        _b('1'),
                        SizedBox(
                          width: 30,
                        ),
                        _b('     Zhalfrezi'),
                        SizedBox(
                          width: 32,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              quantity++;
                            });
                          },
                          child: Container(
                            color: Colors.blue,
                            padding: EdgeInsets.symmetric(horizontal: 6),
                            child: Text(
                              '+',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        _b('  $quantity  '),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              quantity--;
                            });
                          },
                          child: Container(
                            color: Colors.red,
                            padding: EdgeInsets.symmetric(horizontal: 6),
                            child: Text(
                              '-',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 19,
                        ),
                        _b('477'),
                        SizedBox(
                          width: 19,
                        ),
                        _b('1042'),
                        SizedBox(
                          width: 19,
                        ),
                        Icon(
                          Icons.delete_forever,
                          color: Colors.red,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 46,
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  color: Colors.white,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        Container(
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            decoration: BoxDecoration(
                              border: Border(
                                right: BorderSide(color: Colors.grey, width: 2),
                              ),
                            ),
                            child: _b('  1  ')),
                        _b('  Zhalfrezi'),
                        SizedBox(
                          width: 32,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              quantity++;
                            });
                          },
                          child: Container(
                            color: Colors.blue,
                            padding: EdgeInsets.symmetric(horizontal: 6),
                            child: Text(
                              '+',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        _b('  $quantity  '),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              quantity--;
                            });
                          },
                          child: Container(
                            color: Colors.red,
                            padding: EdgeInsets.symmetric(horizontal: 6),
                            child: Text(
                              '-',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 19,
                        ),
                        _b('987'),
                        SizedBox(
                          width: 19,
                        ),
                        _b('1942'),
                        SizedBox(
                          width: 19,
                        ),
                        Icon(
                          Icons.delete_forever,
                          color: Colors.red,
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(''),
                    Flexible(
                      child: FittedBox(
                        child: Text(
                          'Sub Total: 43210 NPR',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w500,
                              color: Colors.black87),
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  height: 80,
                  margin: EdgeInsets.only(bottom: 14),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.grey, width: 2)),
                  child: TextField(
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0),
                      ),
                      hintText: 'Note',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0),
                      ),
                      border: OutlineInputBorder(),
                    ),
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Narrow',
                        fontSize: 16),
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [_c('Place Order'), _c('Print Invoice')],
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [_c('Place Order'), _c('Print Invoice')],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _c(String c) {
    final width = MediaQuery.of(context).size.width;
    return Container(
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 14),
      color: Colors.blue,
      margin: EdgeInsets.only(bottom: 8, right: 2),
      alignment: Alignment.center,
      child: Text(
        c,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  Widget _b(String b) {
    return Text(
      b,
      style: TextStyle(
          fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black87),
    );
  }

  Widget _a(String a) {
    return Container(
      height: 46,
      padding: EdgeInsets.symmetric(horizontal: 12),
      decoration: BoxDecoration(
          border: Border(
            right: BorderSide(color: Colors.white, width: 2),
          ),
          color: Colors.blueGrey),
      alignment: Alignment.center,
      child: Text(
        a,
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w500, fontSize: 16),
      ),
    );
  }

  Widget _mains() {
    final width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        width: width - (width / 2.3),
        child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: width < 600 ? 2 : 3,
                childAspectRatio: 8 / 9),
            itemCount: 10,
            itemBuilder: (context, index) => Container(
                  color: Colors.red,
                  child: GridTile(
                    child: Image.asset(
                      'assets/images/si_sticker.JPG',
                      fit: BoxFit.cover,
                    ),
                    footer: GridTileBar(
                      backgroundColor: Colors.black38,
                      title: Column(
                        children: [
                          Text(
                            'French Fries',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            '140.00',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                            ),
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    ),
                  ),
                )),
      ),
    );
  }

  Widget mainss() {
    final width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 14),
      child: Card(
        elevation: 5,
        borderOnForeground: true,
        shadowColor: Colors.red,
        child: Container(
          width: width * 40 / 100 - 92,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          margin: EdgeInsets.symmetric(horizontal: 22),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                _dropDown('Bill To', "--Walk In Customer--", true),
                _dropDown('Select Order Type', "New Order", false),
                _dropDown('Table Name/NUmber', '--Select Table--', false),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _dropDown(String title, String dd, bool f) {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        f
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(''),
                  Text(
                    'Orders#',
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: 18,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              )
            : Container(),
        Text(
          title,
          style: TextStyle(
              color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w500),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8),
          margin: EdgeInsets.only(top: 6, bottom: 12),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 1),
              borderRadius: BorderRadius.circular(4)),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: Text(dd),
              // Not necessary for Option 1
              value: _selectedLocation,
              onChanged: (newValue) {
                setState(() {
                  _selectedLocation != newValue;
                });
              },
              items: _locations.map((location) {
                return DropdownMenuItem(
                  child: new Text(location),
                  value: location,
                );
              }).toList(),
              isExpanded: true,
            ),
          ),
        ),
      ],
    ));
  }

  Widget _container(String str) {
    return Container(
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 12),
      margin: EdgeInsets.symmetric(horizontal: 6),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 1.4),
          borderRadius: BorderRadius.circular(4)),
      alignment: Alignment.center,
      child: Text(
        str,
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18),
      ),
    );
  }
}
