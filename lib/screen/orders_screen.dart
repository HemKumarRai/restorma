import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restorma/screen/password_recover_screen.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen>
    with TickerProviderStateMixin {
  TabController? tabController;

  @override
  void initState() {
    tabController = new TabController(length: 7, vsync: this);
    super.initState();

    // print(userProfile);
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.grey,
      body: Container(
        height: height,
        width: width,
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  height: 80,
                  width: width,
                  color: Colors.red,
                  alignment: Alignment.centerRight,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        _container('Orders'),
                        _container('Take Orders'),
                        GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          PasswordRecoverScreen(
                                            key: widget.key,
                                          )));
                            },
                            child: _container('Welcome Mr. Bhatta'))
                      ],
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  height: height - 80,
                  width: width,
                  margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Recent Orders',
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              fontSize: 17),
                        ),
                        Divider(
                          color: Colors.grey,
                          thickness: 1.4,
                        ),
                        Container(
                          height: 40,
                          child: TabBar(
                            controller: tabController,
                            labelColor: Colors.white,
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16),
                            unselectedLabelColor: Colors.black,
                            indicator: BoxDecoration(color: Colors.blue),
                            isScrollable: true,
                            tabs: <Widget>[
                              Tab(text: 'Pending'),
                              Tab(
                                text: 'Cooking',
                              ),
                              Tab(
                                text: 'Ready',
                              ),
                              Tab(
                                text: 'Served',
                              ),
                              Tab(
                                text: 'Paid',
                              ),
                              Tab(
                                text: 'Rooms',
                              ),
                              Tab(
                                text: 'Search',
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        Container(
                            height: width,
                            child: TabBarView(
                              controller: tabController,
                              children: [
                                _pending(),
                                Text('Cooking'),
                                Text('Ready'),
                                Text('Served'),
                                Text('Paid'),
                                Text('Rooms'),
                                Text('Search')
                              ],
                            )),
                        SizedBox(
                          height: 54,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _container(String str) {
    return Container(
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 12),
      margin: EdgeInsets.symmetric(horizontal: 6),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 1.4),
          borderRadius: BorderRadius.circular(4)),
      alignment: Alignment.center,
      child: Text(
        str,
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18),
      ),
    );
  }

  Widget _pending() {
    List<String> _oNo = [
      '23',
      '24',
      '22',
      '12',
      '21',
      '44',
      '32',
      '11',
      '33',
      '33',
      '09'
    ];
    final width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.white,
      width: width,
      child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 3,
              childAspectRatio: 8 / 9),
          physics: NeverScrollableScrollPhysics(),
          itemCount: 5,
          itemBuilder: (context, index) => Container(
                decoration: BoxDecoration(
                    color: Colors.blueGrey,
                    image: DecorationImage(
                        image: AssetImage('assets/images/tbl.png'),
                        fit: BoxFit.fill)),
                child: Stack(
                  children: [
                    Positioned(
                      top: 2,
                      left: 50,
                      child: Text(
                        'Order ${_oNo[index]}:#',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 16),
                      ),
                    ),
                    Positioned(
                      right: 2,
                      top: 80,
                      child: Text(
                        'Table: 1',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 14),
                      ),
                    ),
                    Positioned(
                      bottom: 2,
                      left: 16,
                      child: Column(
                        children: [
                          Text(
                            'Customer: Walk In',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            'Status: Pending',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(vertical: 4),
                                color: Colors.blueGrey,
                                child: Text(
                                  'Total',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(vertical: 4),
                                color: Colors.blueGrey,
                                margin: EdgeInsets.symmetric(horizontal: 8),
                                child: Text(
                                  'Ready',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(vertical: 4),
                                color: Colors.blueGrey,
                                child: Text(
                                  'Served',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )),
    );
  }
}
