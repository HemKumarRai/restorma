import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restorma/helper/snackbar.dart';
import 'package:restorma/repositories/user_repo.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PasswordRecoverScreen extends StatefulWidget {
  const PasswordRecoverScreen({Key? key}) : super(key: key);

  @override
  _PasswordRecoverScreenState createState() => _PasswordRecoverScreenState();
}

class _PasswordRecoverScreenState extends State<PasswordRecoverScreen> {
  String? token;
  bool isLoading = false;
  String invalidMessage = "";

  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      token = prefs.getString('token');
    });

    return token;
  }

  TextEditingController _oldPw = new TextEditingController();
  TextEditingController _newPw = new TextEditingController();
  TextEditingController _confirmPw = new TextEditingController();

  @override
  void initState() {
    getStringValuesSF();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        height: height,
        width: width,
        child: ListView(
          children: [
            Column(
              children: [
                Container(
                  height: 100,
                  color: Colors.red,
                  alignment: Alignment.center,
                  width: width,
                  child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 70,
                            width: 100,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/images/abc.JPG'),
                                    fit: BoxFit.cover)),
                          ),
                          SizedBox(
                            width: width / 3,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              _container('Orders'),
                              _container('Take Orders'),
                              Container(
                                height: 40,
                                padding: EdgeInsets.symmetric(horizontal: 12),
                                margin: EdgeInsets.symmetric(horizontal: 6),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.white, width: 1.4),
                                    borderRadius: BorderRadius.circular(4)),
                                alignment: Alignment.center,
                                child: Row(
                                  children: [
                                    Text(
                                      'Welcome User',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18),
                                    ),
                                    Icon(
                                      Icons.arrow_drop_down_sharp,
                                      color: Colors.grey,
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ],
                      )),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: width / 2,
                        margin: EdgeInsets.only(right: 15),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 100,
                              width: 100,
                              margin: EdgeInsets.only(top: 56, bottom: 8),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border:
                                      Border.all(color: Colors.grey, width: 3)),
                              child: Container(
                                height: 85,
                                width: 85,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.grey, width: 2),
                                    image: DecorationImage(
                                        image:
                                            AssetImage('assets/images/bg.png'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            Text(
                              'AB Group (P.) Ltd.',
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 19),
                            ),
                            Text(
                              'Waiter\n\n',
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 17),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 19, left: 8),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 6),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey, width: 2))),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Flexible(
                                    child: FittedBox(
                                      child: Text(
                                        'Phone',
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 17),
                                      ),
                                    ),
                                  ),
                                  Text(
                                    '9779808000909  ',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 6),
                              margin: EdgeInsets.only(bottom: 16, left: 8),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey, width: 2))),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    'Email',
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 17),
                                  ),
                                  Flexible(
                                    child: FittedBox(
                                      child: Text(
                                        'abgroup@gmail.com  ',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: width / 2,
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                        decoration: BoxDecoration(
                            border: Border(
                                left:
                                    BorderSide(color: Colors.black, width: 2))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Change Password',
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 19),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 60,
                                  color: Colors.blue,
                                ),
                                Container(
                                  height: 8,
                                  width: 50,
                                  color: Colors.red,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 80,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Current Password",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1,
                                      fontSize: 17),
                                ),
                                Container(
                                  height: 50,
                                  margin: EdgeInsets.only(top: 8, bottom: 14),
                                  width: width / 2,
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.4),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: TextField(
                                    controller: _oldPw,
                                    decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.green, width: 2.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xffB8C1EC),
                                            width: 2.0),
                                      ),
                                      border: OutlineInputBorder(),
                                    ),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Narrow',
                                        fontSize: 16),
                                  ),
                                )
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "New Password",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1,
                                      fontSize: 17),
                                ),
                                Container(
                                  height: 50,
                                  margin: EdgeInsets.only(top: 8, bottom: 14),
                                  width: width / 2,
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.4),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: TextField(
                                    controller: _newPw,
                                    decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.green, width: 2.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xffB8C1EC),
                                            width: 2.0),
                                      ),
                                      border: OutlineInputBorder(),
                                    ),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Narrow',
                                        fontSize: 16),
                                  ),
                                )
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Confirm Password",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1,
                                      fontSize: 17),
                                ),
                                Container(
                                  height: 50,
                                  margin: EdgeInsets.only(top: 8, bottom: 14),
                                  width: width / 2,
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.4),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: TextField(
                                    controller: _confirmPw,
                                    decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.green, width: 2.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xffB8C1EC),
                                            width: 2.0),
                                      ),
                                      border: OutlineInputBorder(),
                                    ),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Narrow',
                                        fontSize: 16),
                                  ),
                                )
                              ],
                            ),
                            isLoading
                                ? Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : GestureDetector(
                                    onTap: () {
                                      validatedFields(context, token);
                                    },
                                    child: Container(
                                      width: width < 550 ? 310 : width / 2,
                                      height: 66,
                                      margin:
                                          EdgeInsets.symmetric(vertical: 14),
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius: BorderRadius.circular(4),
                                      ),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Submit',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 21,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _container(String str) {
    return Container(
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 12),
      margin: EdgeInsets.symmetric(horizontal: 6),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 1.4),
          borderRadius: BorderRadius.circular(4)),
      alignment: Alignment.center,
      child: Text(
        str,
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18),
      ),
    );
  }

  Widget _column(String txt) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          txt,
          style: TextStyle(
              color: Colors.black87,
              fontWeight: FontWeight.w500,
              letterSpacing: 1,
              fontSize: 17),
        ),
        Container(
          height: 50,
          margin: EdgeInsets.only(top: 8, bottom: 14),
          width: width / 2,
          decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.4),
              borderRadius: BorderRadius.circular(8)),
          child: TextField(
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green, width: 2.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xffB8C1EC), width: 2.0),
              ),
              border: OutlineInputBorder(),
            ),
            style: TextStyle(
                color: Colors.black, fontFamily: 'Narrow', fontSize: 16),
          ),
        )
      ],
    );
  }

  Future<void> validatedFields(BuildContext context, token) async {
    var queryParams = {"token": token};
    String queryString = Uri(queryParameters: queryParams).query;
    String url = "https://restorma.com/api/change-pwd?";
    try {
      setState(() {
        isLoading = true;
      });
      int statusCode = await Repository().changePassword(
          context, _oldPw.text, _newPw.text, url + "token=$token");

      if (statusCode == 200) {
        showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text('An Error Occurred!'),
                  content: Text('Password Changes Successfully!!'),
                  actions: [
                    FlatButton(
                      child: Text('okay'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ],
                ));

        // password changed
        if (statusCode == 200) {
        } else {
          ShowSnackBar().showSnackBar(context, "UnAuthorized Access");
        }
      } else if (statusCode == 401) {
        // invalid credential
        setState(() {
          invalidMessage = "Invalid username or password";
        });
        // ShowSnackBar().showSnackBar(context, "Invalid username or password");
        // print("Username or Password do not match");
      } else if (statusCode >= 500) {
        ShowSnackBar().showSnackBar(context, "Currently server is down");
      } else {
        ShowSnackBar().showSnackBar(
            context, "Something went wrong: " + statusCode.toString());
      }

      setState(() {
        isLoading = false;
      });
    } catch (e) {
      print(e.toString());
    }
  }
}
