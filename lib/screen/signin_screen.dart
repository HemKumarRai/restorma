import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:restorma/helper/snackbar.dart';
import 'package:restorma/repositories/user_repo.dart';
import 'package:restorma/screen/home_page.dart';
import 'package:restorma/screen/password_recover_screen.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  TextEditingController _email = new TextEditingController();
  TextEditingController _password = new TextEditingController();
  TextEditingController _usercode = new TextEditingController();
  bool isLoading = false;

  String invalidMessage = "";

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: height,
              width: width / 2.3,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        'assets/images/si_sticker.JPG',
                      ),
                      fit: BoxFit.fill)),
            ),
            Container(
              height: height,
              width: width - (width / 2.3),
              padding: EdgeInsets.symmetric(horizontal: 12),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 200,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image:
                                  AssetImage('assets/images/si_sticker2.JPG'))),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Email',
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1,
                              fontSize: 17),
                        ),
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(top: 8, bottom: 14),
                          width: width / 2,
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.4),
                              borderRadius: BorderRadius.circular(8)),
                          child: TextField(
                            controller: _email,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.green, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffB8C1EC), width: 2.0),
                              ),
                              border: OutlineInputBorder(),
                            ),
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Narrow',
                                fontSize: 16),
                          ),
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Password',
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1,
                              fontSize: 17),
                        ),
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(top: 8, bottom: 14),
                          width: width / 2,
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.4),
                              borderRadius: BorderRadius.circular(8)),
                          child: TextField(
                            controller: _password,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.green, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffB8C1EC), width: 2.0),
                              ),
                              border: OutlineInputBorder(),
                            ),
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Narrow',
                                fontSize: 16),
                          ),
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "UserCode",
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1,
                              fontSize: 17),
                        ),
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(top: 8, bottom: 14),
                          width: width / 2,
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.4),
                              borderRadius: BorderRadius.circular(8)),
                          child: TextField(
                            controller: _usercode,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.green, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffB8C1EC), width: 2.0),
                              ),
                              border: OutlineInputBorder(),
                            ),
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Narrow',
                                fontSize: 16),
                          ),
                        )
                      ],
                    ),
                    isLoading
                        ? Center(child: CircularProgressIndicator())
                        : GestureDetector(
                            onTap: () {
                              validatedFields(context);
                            },
                            child: Container(
                              width: width < 550 ? 310 : width / 2,
                              height: 66,
                              margin: EdgeInsets.symmetric(vertical: 14),
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(4),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                'Log In',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 21,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> validatedFields(BuildContext context) async {
    print(_email.text);
    print(_password.text);
    print(_usercode.text);
    try {
      setState(() {
        isLoading = true;
      });
      int statusCode = await Repository()
          .login(context, _email.text, _password.text, _usercode.text);

      if (statusCode == 200) {
        // login success

        // password changed
        if (statusCode == 200) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HomeScreen(
                        key: widget.key,
                      )));
        } else {
          ShowSnackBar().showSnackBar(context, "UnAuthorized Access");
        }
      } else if (statusCode == 401) {
        // invalid credential
        setState(() {
          invalidMessage = "Invalid username or password";
        });
        // ShowSnackBar().showSnackBar(context, "Invalid username or password");
        // print("Username or Password do not match");
      } else if (statusCode >= 500) {
        ShowSnackBar().showSnackBar(context, "Currently server is down");
      } else {
        ShowSnackBar().showSnackBar(
            context, "Something went wrong: " + statusCode.toString());
      }

      setState(() {
        isLoading = false;
      });
    } catch (e) {
      print(e.toString());
    }
  }
}
